//Step 2: change the base code to use Component
import React, { Component } from 'react'
import Web3 from 'web3'
import './App.css';
// Step 3: import the ABI
import { STUDENTRECORD_ABI, STUDENTRECORD_ADDRESS } from './abi/config_ studentrecord'
import StudentRecord from './components/StudentRecord';

//We replace function with a component for better management of code
class App extends Component {
//componentDidMount called once on the client after the initial render
//when the client receives data from the server and before the data is displayed.
componentDidMount() {
  if (!window.ethereum)
  throw new Error("No crypto wallet found. Please install it.");
  window.ethereum.send("eth_requestAccounts");
  this.loadBlockchainData()
}

//Initialise the variables stored in the state
constructor(props) {
  super(props)
  this.state = {
  account: '',
  studentCount: 0,
  students: [],
  loading: true
  }
  this.addStudent = this.addStudent.bind(this)
  this.markGraduated = this.markGraduated.bind(this)
}  
//create a web3 connection to using a provider, MetaMask to connect to
// //the ganache server and retrieve the first account
async loadBlockchainData() {
  const web3 = new Web3(Web3.givenProvider || "http://localhost:7545")
  const accounts = await web3.eth.getAccounts()
  this.setState({ account: accounts[0] })
  //Step 4: load all the lists from the blockchain
  const studentRecord = new web3.eth.Contract(STUDENTRECORD_ABI, STUDENTRECORD_ADDRESS)
  //Keep the lists in the current state
  this.setState({ studentRecord })
  //Get the number of records for all list in the blockchain
  const studentCount = await studentRecord.methods.studentsCount().call()
  //Store this value in the current state as well
  this.setState({ studentCount })
  //Use an iteration to extract each record info and store
  //them in an array
  
  this.setState.students=[]
  for (var i = 1; i <= studentCount; i++) {
  const student = await studentRecord.methods.students(i).call()
  this.setState({
  students: [...this.state.students, student]
  })
  }
}
addStudent(sid,name) {
  this.setState({ loading: true })
  this.state.studentRecord.methods.addStudent(sid,name)
  .send({ from: this.state.account,
  gas: 2100000})
  .once('receipt', (receipt) => {
  this.setState({ loading: false })
  this.loadBlockchainData()
  })
  .on('error', (error) => {
    console.error(error.message);
    this.setState({ loading: false });
  });
  }

markGraduated(sid) {
  this.setState({ loading: true })
  this.state.studentRecord.methods.markGraduated(sid)
  .send({ from: this.state.account })
  .once('receipt', (receipt) => {
  this.setState({ loading: false })
  })
  }
    

  
//Display the first account
//Display the first account and student detail
render() {
  return (
    <div className="container">
      <h1>Student Record System</h1>
      <p>Your account: {this.state.account}</p>
      
      <StudentRecord  addStudent={this.addStudent}/>
      <ul id="studentList" className="list-unstyled">
      {
        //This gets the each student from the studentList and pass them into a function that display the details of the student
        this.state.students.map((student, key) => {
          return (
            <li className="list-group-item checkbox" key={key}>
            <span className="name alert">{student._id} {student.sid} {student.name}</span>
            <input
            className="form-check-input"
            type="checkbox"
            name={student._id}
            defaultChecked={student.graduated}
            disabled={student.graduated}
            ref={(input) => {
            this.checkbox = input
            }}
            onClick={(event) => {
              this.markGraduated(event.currentTarget.name)}}
            />
            <label className="form-check-label" >Graduated</label>
            <span className="graduation-info">
                {student.graduated && `Graduated on ${new Date(student.graduationTimestamp * 1000).toLocaleString()}`}
              </span>
            </li>
          )
        }
        )
      }
      </ul>
    </div>
    );
  }
}
export default App;




// // import React, { Component } from 'react';
// // import Web3 from 'web3';
// // import './App.css';
// // import StudentRecord from './components/StudentRecord';

// // import { STUDENTRECORD_ABI, STUDENTRECORD_ADDRESS } from './abi/config_ studentrecord';

// // class App extends Component {
// //   constructor(props) {
// //     super(props);
// //     this.state = {
// //       account: '',
// //       studentCount: 0,
// //       students: [],
// //       loading: true,
// //     };
// //     this.markGraduated = this.markGraduated.bind(this)
// //   }
// //   markGraduated = async (sid) => {
// //     this.setState({ loading: true });
  
// //     try {
// //       // Estimate the gas required for the transaction
// //       const gasEstimate = await this.state.studentRecord.methods.markGraduated(sid).estimateGas({
// //         from: this.state.account
// //       });
  
// //       // console.log(Estimated gas usage: ${gasEstimate});
  
// //       // Send the transaction with the estimated gas
// //       this.state.studentRecord.methods.markGraduated(sid)
// //         .send({
// //           from: this.state.account,
// //           gas: gasEstimate, // Use the estimated gas
// //         })
// //         .on('receipt', (receipt) => {
// //           this.setState({ loading: false });
// //           this.loadBlockchainData();
// //         })
// //         .on('error', (error) => {
// //           console.error(error.message);
// //           this.setState({ loading: false });
// //         });
// //     } catch (error) {
// //       console.error("Error estimating gas:", error);
// //       this.setState({ loading: false });
// //     }
// //   }
  
// //   // markGraduated(sid) {
// //   //   this.setState({ loading: true })
// //   //   this.state.studentList.methods.markGraduated(sid).send({ from: this.state.account }).once('receipt', (receipt) => {
// //   //     this.setState({ loading: false })
// //   //     this.loadBlockchainData()
// //   //   })
// //   // }

// //   async componentDidMount() {
// //     try {
// //       if (!window.ethereum) {
// //         throw new Error("No crypto wallet found. Please install it.");
// //       }

// //       await window.ethereum.send("eth_requestAccounts");
// //       this.loadBlockchainData();
// //     } catch (error) {
// //       console.error(error.message);
// //     }
// //   }

// //   async loadBlockchainData() {
// //     try {
// //       const web3 = new Web3(Web3.givenProvider || "http://localhost:7545");
// //       const accounts = await web3.eth.getAccounts();
// //       this.setState({ account: accounts[0] });

// //       const studentRecord = new web3.eth.Contract(STUDENTRECORD_ABI, STUDENTRECORD_ADDRESS);
// //       this.setState({ studentRecord });

// //       const studentCount = await studentRecord.methods.studentsCount().call();
// //       this.setState({ studentCount });

// //       const students = [];
// //       for (var i = 1; i <= studentCount; i++) {
// //         const student = await studentRecord.methods.students(i).call();
// //         students.push(student);
// //       }
// //       this.setState({ students, loading: false });
// //     } catch (error) {
// //       console.error(error.message);
// //     }
// //   }

// //   addStudent = async (sid, name) => {
// //     this.setState({ loading: true });
// //   //Added gas fee estimation extra
// //     try {
// //       // Estimate the gas required for the transaction
// //       const gasEstimate = await this.state.studentRecord.methods.addStudent(sid, name)
// //         .estimateGas({ from: this.state.account });
  
// //       // console.log(Estimated gas usage: ${gasEstimate});
  
// //       // Send the transaction with the estimated gas
// //       this.state.studentRecord.methods.addStudent(sid, name)
// //         .send({
// //           from: this.state.account,
// //           gas: gasEstimate, // Use the estimated gas
// //         })
// //         .on('receipt', (receipt) => {
// //           this.setState({ loading: false });
// //           this.loadBlockchainData();
// //         })
// //         .on('error', (error) => {
// //           console.error(error.message);
// //           this.setState({ loading: false });
// //         });
// //     } catch (error) {
// //       console.error("Error estimating gas:", error);
// //       this.setState({ loading: false });
// //     }
// //   }
  
// //   // addStudent = (sid, name) => {
// //   //   this.setState({ loading: true });
  
// //   //   this.state.studentRecord.methods.addStudent(sid, name)
// //   //     .send({ from: this.state.account })
// //   //     .on('receipt', (receipt) => {
// //   //       this.setState({ loading: false });
// //   //       this.loadBlockchainData();
// //   //     })
// //   //     .on('error', (error) => {
// //   //       console.error(error.message);
// //   //       this.setState({ loading: false });
// //   //     });
// //   // }
// //   render() {
// //     return (
// //       <div className="container">
// //         <h1>Student Record System</h1>
// //         <p>Your account: {this.state.account}</p>
// //         <StudentRecord addStudent={this.addStudent} />
// //         <ul id="studentList" className="list-unstyled">
// //           {this.state.students.map((student, key) => (
// //             <li className="list-group-item checkbox" key={key}>
// //               <span className="name alert">
// //                 {student._id}. {student.sid} {student.name}
// //               </span>
// //               <input
// //                 className="form-check-input"
// //                 type="checkbox"
// //                 name={student._id}
// //                 defaultChecked={student.graduated}
// //                 disabled={student.graduated}
// //                 ref={(input) => {
// //                   this.checkbox = input;
// //                 }}onClick={(event) => {
// //                   this.markGraduated(event.currentTarget.name)}
// //                 }/>
// //               <label className="form-check-label">Graduated</label>
// //             </li>
// //           ))}
// //         </ul>
// //       </div>
// //     );
// //   }
// // }

// // export default App;


// import React, { Component } from 'react';
// import Web3 from 'web3';
// import './App.css';
// import { STUDENTRECORD_ABI, STUDENTRECORD_ADDRESS } from './abi/config_ studentrecord'
// import StudentRecord from './components/StudentRecord';

// class App extends Component {
//   state = {
//     account: '',
//     studentCount: 0,
//     students: [],
//     loading: true,
//     studentRecord: null,
//   };

//   async componentDidMount() {
//     try {
//       if (!window.ethereum) throw new Error("No crypto wallet found. Please install it.");
//       await window.ethereum.send("eth_requestAccounts");

//       const web3 = new Web3(Web3.givenProvider || "http://localhost:7545");
//       const accounts = await web3.eth.getAccounts();
//       this.setState({ account: accounts[0] });

//       const studentRecord = new web3.eth.Contract(STUDENTRECORD_ABI, STUDENTRECORD_ADDRESS);
//       this.setState({ studentRecord });

//       this.loadBlockchainData();
//     } catch (error) {
//       console.error("Error in componentDidMount:", error.message);
//     }
//   }

//   async loadBlockchainData() {
//     try {
//       const studentCount = await this.state.studentRecord.methods.studentsCount().call();
//       this.setState({ studentCount });

//       const students = [];
//       for (let i = 1; i <= studentCount; i++) {
//         const student = await this.state.studentRecord.methods.students(i).call();
//         students.push(student);
//       }
//       this.setState({ students, loading: false });
//     } catch (error) {
//       console.error("Error in loadBlockchainData:", error.message);
//     }
//   }

//   async addStudent(sid, name) {
//     try {
//       this.setState({ loading: true });
//       await this.state.studentRecord.methods.addStudent(sid, name)
//         .send({ from: this.state.account, gas: 2100000 });

//       // Reload data after transaction receipt
//       this.loadBlockchainData();
//     } catch (error) {
//       console.error("Error in addStudent:", error.message);
//       this.setState({ loading: false });
//     }
//   }

//   async markGraduated(sid) {
//     try {
//       this.setState({ loading: true });
//       const result = await this.state.studentRecord.methods.markGraduated(sid)
//         .send({ from: this.state.account });

//       // Log the result to the console
//       console.log("Mark Graduated Result:", result);

//       // Reload data after transaction receipt
//       this.loadBlockchainData();
//     } catch (error) {
//       console.error("Error in markGraduated:", error.message);
//       this.setState({ loading: false });
//     }
//   }

//   render() {
//     return (
//       <div className="container">
//         <h1>Student Record System</h1>
//         <p>Your account: {this.state.account}</p>

//         {/* Pass addStudent and markGraduated functions as props to StudentRecord component */}
//         <StudentRecord addStudent={(sid, name) => this.addStudent(sid, name)} />

//         <ul id="studentList" className="list-unstyled">
//           {this.state.students.map((student, key) => (
//             <li className="list-group-item checkbox" key={key}>
//               <span className="name alert">{student._id} {student.sid} {student.name}</span>
//               <span className="graduation-info">
//                 {student.graduated && `Graduated on ${new Date(student.graduationTimestamp * 1000).toLocaleString()}`}
//               </span>
//               <input
//                 className="form-check-input"
//                 type="checkbox"
//                 name={student._id}
//                 defaultChecked={student.graduated}
//                 disabled={student.graduated}
//                 ref={(input) => { this.checkbox = input; }}
//                 onClick={() => this.markGraduated(student.sid)}
//               />
//               <label className="form-check-label">Graduated</label>
//             </li>
//           ))}
//         </ul>
//       </div>
//     );
//   }
// }

// export default App;
