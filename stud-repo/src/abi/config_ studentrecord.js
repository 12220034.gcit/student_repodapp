export const STUDENTRECORD_ADDRESS = '0xeA5c3457E6Fbeee0cCe77efFafd198491340dea5'
export const STUDENTRECORD_ABI = [
  {
    "inputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "constructor"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": false,
        "internalType": "uint256",
        "name": "_id",
        "type": "uint256"
      },
      {
        "indexed": true,
        "internalType": "uint256",
        "name": "sid",
        "type": "uint256"
      },
      {
        "indexed": false,
        "internalType": "string",
        "name": "name",
        "type": "string"
      },
      {
        "indexed": false,
        "internalType": "bool",
        "name": "graduated",
        "type": "bool"
      }
    ],
    "name": "addStudentEvent",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": true,
        "internalType": "uint256",
        "name": "sid",
        "type": "uint256"
      },
      {
        "indexed": false,
        "internalType": "uint256",
        "name": "graduationTimestamp",
        "type": "uint256"
      }
    ],
    "name": "markGraduatedEvent",
    "type": "event"
  },
  {
    "constant": true,
    "inputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "name": "students",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "_id",
        "type": "uint256"
      },
      {
        "internalType": "uint256",
        "name": "sid",
        "type": "uint256"
      },
      {
        "internalType": "string",
        "name": "name",
        "type": "string"
      },
      {
        "internalType": "bool",
        "name": "graduated",
        "type": "bool"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  {
    "constant": true,
    "inputs": [],
    "name": "studentsCount",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  {
    "constant": false,
    "inputs": [
      {
        "internalType": "uint256",
        "name": "_studentNumber",
        "type": "uint256"
      },
      {
        "internalType": "string",
        "name": "_name",
        "type": "string"
      }
    ],
    "name": "addStudent",
    "outputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "_id",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "sid",
            "type": "uint256"
          },
          {
            "internalType": "string",
            "name": "name",
            "type": "string"
          },
          {
            "internalType": "bool",
            "name": "graduated",
            "type": "bool"
          }
        ],
        "internalType": "struct StudentRecord.Student",
        "name": "",
        "type": "tuple"
      }
    ],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "constant": false,
    "inputs": [
      {
        "internalType": "uint256",
        "name": "_id",
        "type": "uint256"
      }
    ],
    "name": "markGraduated",
    "outputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "_id",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "sid",
            "type": "uint256"
          },
          {
            "internalType": "string",
            "name": "name",
            "type": "string"
          },
          {
            "internalType": "bool",
            "name": "graduated",
            "type": "bool"
          }
        ],
        "internalType": "struct StudentRecord.Student",
        "name": "",
        "type": "tuple"
      }
    ],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "constant": true,
    "inputs": [
      {
        "internalType": "uint256",
        "name": "_id",
        "type": "uint256"
      }
    ],
    "name": "findStudent",
    "outputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "_id",
            "type": "uint256"
          },
          {
            "internalType": "uint256",
            "name": "sid",
            "type": "uint256"
          },
          {
            "internalType": "string",
            "name": "name",
            "type": "string"
          },
          {
            "internalType": "bool",
            "name": "graduated",
            "type": "bool"
          }
        ],
        "internalType": "struct StudentRecord.Student",
        "name": "",
        "type": "tuple"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  }
];

// export const STUDENTRECORD_ABI = [
//     {
//       "inputs": [],
//       "payable": false,
//       "stateMutability": "nonpayable",
//       "type": "constructor"
//     },
//     {
//       "anonymous": false,
//       "inputs": [
//         {
//           "indexed": false,
//           "internalType": "uint256",
//           "name": "_id",
//           "type": "uint256"
//         },
//         {
//           "indexed": true,
//           "internalType": "uint256",
//           "name": "sid",
//           "type": "uint256"
//         },
//         {
//           "indexed": false,
//           "internalType": "string",
//           "name": "name",
//           "type": "string"
//         },
//         {
//           "indexed": false,
//           "internalType": "bool",
//           "name": "graduated",
//           "type": "bool"
//         }
//       ],
//       "name": "addStudentEvent",
//       "type": "event"
//     },
//     {
//       "anonymous": false,
//       "inputs": [
//         {
//           "indexed": true,
//           "internalType": "uint256",
//           "name": "sid",
//           "type": "uint256"
//         }
//       ],
//       "name": "markGraduatedEvent",
//       "type": "event"
//     },
//     {
//       "constant": true,
//       "inputs": [
//         {
//           "internalType": "uint256",
//           "name": "",
//           "type": "uint256"
//         }
//       ],
//       "name": "students",
//       "outputs": [
//         {
//           "internalType": "uint256",
//           "name": "_id",
//           "type": "uint256"
//         },
//         {
//           "internalType": "uint256",
//           "name": "sid",
//           "type": "uint256"
//         },
//         {
//           "internalType": "string",
//           "name": "name",
//           "type": "string"
//         },
//         {
//           "internalType": "bool",
//           "name": "graduated",
//           "type": "bool"
//         }
//       ],
//       "payable": false,
//       "stateMutability": "view",
//       "type": "function"
//     },
//     {
//       "constant": true,
//       "inputs": [],
//       "name": "studentsCount",
//       "outputs": [
//         {
//           "internalType": "uint256",
//           "name": "",
//           "type": "uint256"
//         }
//       ],
//       "payable": false,
//       "stateMutability": "view",
//       "type": "function"
//     },
//     {
//       "constant": false,
//       "inputs": [
//         {
//           "internalType": "uint256",
//           "name": "_studentNumber",
//           "type": "uint256"
//         },
//         {
//           "internalType": "string",
//           "name": "_name",
//           "type": "string"
//         }
//       ],
//       "name": "addStudent",
//       "outputs": [
//         {
//           "components": [
//             {
//               "internalType": "uint256",
//               "name": "_id",
//               "type": "uint256"
//             },
//             {
//               "internalType": "uint256",
//               "name": "sid",
//               "type": "uint256"
//             },
//             {
//               "internalType": "string",
//               "name": "name",
//               "type": "string"
//             },
//             {
//               "internalType": "bool",
//               "name": "graduated",
//               "type": "bool"
//             }
//           ],
//           "internalType": "struct StudentRecord.Student",
//           "name": "",
//           "type": "tuple"
//         }
//       ],
//       "payable": false,
//       "stateMutability": "nonpayable",
//       "type": "function"
//     },
//     {
//       "constant": false,
//       "inputs": [
//         {
//           "internalType": "uint256",
//           "name": "_id",
//           "type": "uint256"
//         }
//       ],
//       "name": "markGraduated",
//       "outputs": [
//         {
//           "components": [
//             {
//               "internalType": "uint256",
//               "name": "_id",
//               "type": "uint256"
//             },
//             {
//               "internalType": "uint256",
//               "name": "sid",
//               "type": "uint256"
//             },
//             {
//               "internalType": "string",
//               "name": "name",
//               "type": "string"
//             },
//             {
//               "internalType": "bool",
//               "name": "graduated",
//               "type": "bool"
//             }
//           ],
//           "internalType": "struct StudentRecord.Student",
//           "name": "",
//           "type": "tuple"
//         }
//       ],
//       "payable": false,
//       "stateMutability": "nonpayable",
//       "type": "function"
//     },
//     {
//       "constant": true,
//       "inputs": [
//         {
//           "internalType": "uint256",
//           "name": "_id",
//           "type": "uint256"
//         }
//       ],
//       "name": "findStudent",
//       "outputs": [
//         {
//           "components": [
//             {
//               "internalType": "uint256",
//               "name": "_id",
//               "type": "uint256"
//             },
//             {
//               "internalType": "uint256",
//               "name": "sid",
//               "type": "uint256"
//             },
//             {
//               "internalType": "string",
//               "name": "name",
//               "type": "string"
//             },
//             {
//               "internalType": "bool",
//               "name": "graduated",
//               "type": "bool"
//             }
//           ],
//           "internalType": "struct StudentRecord.Student",
//           "name": "",
//           "type": "tuple"
//         }
//       ],
//       "payable": false,
//       "stateMutability": "view",
//       "type": "function"
//     }
//   ]